<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 21/11/2016
 * Time: 12:01
 */

namespace Lshtmweb\MailerLaravel;


use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use Ixudra\Curl\Facades\Curl;

trait SendsMailerEmails
{
        protected $template;
        protected $params;
        /**
         * @var string
         */
        protected $recipient;
        /**
         * @var Collection
         */
        protected $attachments;

        protected $subject;

        public function setTemplate($template)
        {
                $this->template = $template;
        }

        public function setParams($params)
        {
                $this->params = $params;
        }

        public function getTemplate()
        {
                return $this->template;
        }

        /**
         * @return Collection
         */
        public function getParams()
        {
                return $this->params;
        }

        /**
         * @return mixed
         */
        public function getRecipient()
        {
                return $this->recipient;
        }

        /**
         * @param mixed $recipient
         */
        public function setRecipient($recipient)
        {
                $this->recipient = $recipient;
        }

        public function getAttachments()
        {
                return $this->attachments;
        }

        public function getSubject()
        {
                return $this->subject;
        }

        public function setSubject($subject)
        {
                $this->subject = $subject;
        }

        /**
         * @param $value
         */
        public function setAttachments($value)
        {
                if ($this->attachments == null) {
                        $this->attachments = new Collection;
                }
                if (is_array($value)) {
                        foreach ($value as $item) {
                                $this->attachments->push($item);
                        }
                } else {
                        $this->attachments->push($value);
                }
        }

        /**
         * Prepare params for member.
         *
         * @param $params
         *
         * @return string
         */
        public static function prepareParams($params)
        {
                $flattened_params = $params;
                array_walk($flattened_params, function (&$v, $k) {
                        $v = "{$k}={$v}";
                });

                //TODO: Move Delimeter to configurable object.
                return implode(";", $flattened_params);
        }

        /**
         * @param $mailer SendsMailerEmails
         */
        public static function sendEmail($mailer)
        {
                Mail::raw('', function ($m) use ($mailer) {
                        $m->to($mailer->getRecipient(), $mailer->getRecipient())->subject($mailer->getSubject());
                        $m->getSwiftMessage()->mailerObject = $mailer;
                });
        }
}