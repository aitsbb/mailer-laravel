<?php

namespace Lshtmweb\MailerLaravel;

use Illuminate\Mail\MailServiceProvider;

class MailerMailServiceProvider extends MailServiceProvider
{
        /**
         * Bootstrap the application services.
         *
         * @return void
         */
        public function boot()
        {
                $this->publishes([
                    __DIR__ . '/../config/mailer.php' => $this->app->basePath() . '/config/mailer.php'
                ]);
        }

        /**
         * Register the application services.
         *
         * @return void
         */
        public function register()
        {
                $this->registerSwiftMailer();
                $this->registerMigrations();

                $this->app->singleton('mailer-mail', function ($app) {
                        return new MailerMailService($app);
                });
        }

        function registerSwiftMailer()
        {
                if ($this->app['config']['mail.driver'] == 'mailer') {
                        $this->registerMailerSwiftMailer();
                } else {
                        parent::registerSwiftMailer();
                }
        }

        function registerMailerSwiftMailer()
        {
                $this->app->singleton('swift.mailer', function ($app) {
                        $configuration = $app['config']['mailer'];

                        return new \Swift_Mailer(new MailerTransport(new MailerMailService($app), $configuration));
                });
        }

        function registerMigrations()
        {
            $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
                /*$this->publishes([
                    __DIR__.'/../database/migrations' => database_path('migrations'),
                ], 'mailer-migrations');*/
        }
}
